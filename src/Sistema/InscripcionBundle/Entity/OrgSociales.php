<?php

namespace Sistema\InscripcionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * OrgSociales
 *
 * @ORM\Table(name="org_sociales")
 * @ORM\Entity(repositoryClass="Sistema\InscripcionBundle\Repository\OrgSocialesRepository")
 */
class OrgSociales
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255, nullable=false)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="apellido", type="string", length=255, nullable=false)
     */
    private $apellido;

    /**
     * @var integer
     *
     * @ORM\Column(name="matricula", type="integer", nullable=true)
     */
    private $matricula;

    /**
     * @var integer
     *
     * @ORM\Column(name="dni", type="integer", nullable=true)
     */
    private $dni;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     */
    private $email;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="fechanac", type="datetime", nullable=false)
     */
    private $fechanac;

    /**
     * @var string
     *
     * @ORM\Column(name="categoria", type="string", length=75, nullable=false)
     */
    private $categoria;

    /**
     * @var string
     *
     * @ORM\Column(name="area", type="string", length=255, nullable=false)
     */
    private $area;

    /**
     * @var presentatrabajo
     *
     * @ORM\Column(name="presentatrabajo", type="boolean")
     */
    private $presentatrabajo;

    /**
     * @var string
     *
     * @ORM\Column(name="telefono", type="string", length=255, nullable=false)
     */
    private $telefono;

    /**
     * @var int
     *
     * @ORM\Column(name="tipopago", type="integer")
     */
    private $tipopago;
    
    /**
     * @var float
     *
     * @ORM\Column(name="importe", type="float")
     */
    private $importe;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime")
     */
    private $fecha;

    /**
     * @var string
     *
     * @ORM\Column(name="estado", type="string", length=75)
     */
    private $estado;

    /**
     * @var string
     *
     * @ORM\Column(name="menu", type="text", nullable=true)
     */
    private $menu;

    /**
     * @var string
     *
     * @ORM\Column(name="provincia", type="string", length=255, nullable=false)
     */
    private $provincia;

    /**
     * @var string
     *
     * @ORM\Column(name="hotel", type="string", length=255, nullable=false)
     */
    private $hotel;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->fecha = new \DateTime('now');
        $this->estado = "Generado";
        $this->importe = 900;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return OrgSociales
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellido
     *
     * @param string $apellido
     *
     * @return OrgSociales
     */
    public function setApellido($apellido)
    {
        $this->apellido = $apellido;

        return $this;
    }

    /**
     * Get apellido
     *
     * @return string
     */
    public function getApellido()
    {
        return $this->apellido;
    }

    /**
     * Set tipopago
     *
     * @param int $tipopago
     *
     * @return OrgSociales
     */
    public function setTipopago($tipopago)
    {
        $this->tipopago = $tipopago;

        return $this;
    }

    /**
     * Get tipopago
     *
     * @return int
     */
    public function getTipopago()
    {
        return $this->tipopago;
    }

    /**
     * Set importe
     *
     * @param float $importe
     *
     * @return OrgSociales
     */
    public function setImporte($importe)
    {
        $this->importe = $importe;

        return $this;
    }

    /**
     * Get importe
     *
     * @return float
     */
    public function getImporte()
    {
        return $this->importe;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     *
     * @return OrgSociales
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set estado
     *
     * @param string $estado
     *
     * @return OrgSociales
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set matricula
     *
     * @param integer $matricula
     *
     * @return OrgSociales
     */
    public function setMatricula($matricula)
    {
        $this->matricula = $matricula;

        return $this;
    }

    /**
     * Get matricula
     *
     * @return integer
     */
    public function getMatricula()
    {
        return $this->matricula;
    }

    /**
     * Set dni
     *
     * @param integer $dni
     *
     * @return OrgSociales
     */
    public function setDni($dni)
    {
        $this->dni = $dni;

        return $this;
    }

    /**
     * Get dni
     *
     * @return integer
     */
    public function getDni()
    {
        return $this->dni;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return OrgSociales
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set fechanac
     *
     * @param \DateTime $fechanac
     *
     * @return OrgSociales
     */
    public function setFechanac($fechanac)
    {
        $this->fechanac = $fechanac;

        return $this;
    }

    /**
     * Get fechanac
     *
     * @return \DateTime
     */
    public function getFechanac()
    {
        return $this->fechanac;
    }

    /**
     * Set categoria
     *
     * @param string $categoria
     *
     * @return OrgSociales
     */
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;

        return $this;
    }

    /**
     * Get categoria
     *
     * @return string
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * Set area
     *
     * @param string $area
     *
     * @return OrgSociales
     */
    public function setArea($area)
    {
        $this->area = $area;

        return $this;
    }

    /**
     * Get area
     *
     * @return string
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * Set presentatrabajo
     *
     * @param boolean $presentatrabajo
     *
     * @return OrgSociales
     */
    public function setPresentatrabajo($presentatrabajo)
    {
        $this->presentatrabajo = $presentatrabajo;

        return $this;
    }

    /**
     * Get presentatrabajo
     *
     * @return boolean
     */
    public function getPresentatrabajo()
    {
        return $this->presentatrabajo;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     *
     * @return OrgSociales
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set menu.
     *
     * @param string $menu
     *
     * @return OrgSociales
     */
    public function setMenu($menu) {
        $this->menu = $menu;

        return $this;
    }

    /**
     * Get menu.
     *
     * @return string
     */
    public function getMenu() {
        return $this->menu;
    }

    /**
     * Set provincia
     *
     * @param string $provincia
     *
     * @return OrgSociales
     */
    public function setProvincia($provincia)
    {
        $this->provincia = $provincia;

        return $this;
    }

    /**
     * Get provincia
     *
     * @return string
     */
    public function getProvincia()
    {
        return $this->provincia;
    }

    /**
     * Set hotel
     *
     * @param string $hotel
     *
     * @return OrgSociales
     */
    public function setHotel($hotel)
    {
        $this->hotel = $hotel;

        return $this;
    }

    /**
     * Get hotel
     *
     * @return string
     */
    public function getHotel()
    {
        return $this->hotel;
    }

    /**
     * Get apellido y nombre
     *
     * @return string
     */
    public function getApellidoyNombre()
    {
        return $this->apellido . ", " . $this->nombre;
    }

    /**
     * Get matricula estado
     *
     * @return string
     */
    public function getMatriculaEst()
    {
        if ($this->matricula) {
            return (string)$this->matricula;
        } else {
            return "No tiene";
        }
    }

    /**
     * Get Tipo de Trabajo
     *
     * @return string
     */
    public function getTipoTrab()
    {
        switch ($this->tipopago) {
            case 0:
                return "Inscripción + Cena";
                break;
            case 1:
                return "Solo Inscripción";
                break;
            default:
                return "Cena";
                break;
        }
    }
}
