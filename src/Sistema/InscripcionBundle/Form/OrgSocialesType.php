<?php

namespace Sistema\InscripcionBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrgSocialesType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('apellido', null, array(
                'attr' => array(
                    'col' => 'col-lg-6 col-md-6 col-sm-12',
                ),
            ))
            ->add('nombre', null, array(
                'attr' => array(
                    'col' => 'col-lg-6 col-md-6 col-sm-12',
                ),
            ))
            ->add('dni', null, array(
                'label' => 'DNI Nº',
                'attr' => array(
                    'col' => 'col-lg-6 col-md-6 col-sm-12',
                ),
            ))
            ->add('fechanac', \SC\DatetimepickerBundle\Form\Type\DatetimeType::class, [
                'pickerOptions' => [
                    'format'    => 'dd/mm/yyyy',
                    'startView' => 'decade',
                    'minView'   => 'month',
                    'maxView'   => 'decade',
                    'todayBtn'  => true,
                    'initialDate' => date('01/01/1980'),
                ],
                'label' => 'Fecha de Nacimiento',
                'attr' => array(
                    'col' => 'col-lg-6 col-md-6 col-sm-12',
                ),
            ])
            ->add('telefono', null, array(
                'label' => 'Teléfono / Celular',
                'attr' => array(
                    'col' => 'col-lg-6 col-md-6 col-sm-12',
                ),
            ))
            ->add('email', null, array(
                'attr' => array(
                    'col' => 'col-lg-6 col-md-6 col-sm-12',
                ),
            ))
            ->add('provincia', ChoiceType::class, array(
                'label' => 'Provincia',
                'attr' => array(
                    'col' => 'col-lg-6 col-md-6 col-sm-12',
                ),
                'choices'  => array(
                    'Buenos Aires' => 'Buenos Aires',
                    'Catamarca' => 'Catamarca',
                    'Chaco' => 'Chaco',
                    'Chubut' => 'Chubut',
                    'Córdoba' => 'Córdoba',
                    'Corrientes' => 'Corrientes',
                    'Entre Ríos' => 'Entre Ríos',
                    'Formosa' => 'Formosa',
                    'Jujuy' => 'Jujuy',
                    'La Pampa' => 'La Pampa',
                    'La Rioja' => 'La Rioja',
                    'Mendoza' => 'Mendoza',
                    'Misiones' => 'Misiones',
                    'Neuquén' => 'Neuquén',
                    'Río Negro' => 'Río Negro',
                    'Salta' => 'Salta',
                    'San Juan' => 'San Juan',
                    'San Luis' => 'San Luis',
                    'Santa Cruz' => 'Santa Cruz',
                    'Santa Fe' => 'Santa Fe',
                    'Santiago del Estero' => 'Santiago del Estero',
                    'Tierra del Fuego, Antártida e Islas del Atlántico Sur' => 'Tierra del Fuego...',
                    'Tucumán' => 'Tucumán',
                ),
            ))
            ->add('matricula', null, array(
                'label' => 'Matrícula Nº',
                'attr' => array(
                    'col' => 'col-lg-6 col-md-6 col-sm-12',
                ),
            ))
            ->add('categoria', ChoiceType::class, array(
                'label' => 'Categoría',
                'attr' => array(
                    'col' => 'col-lg-6 col-md-6 col-sm-12',
                ),
                'choices'  => array(
                    'Participante' => 'Participante',
                    'Acompañante' => 'Acompañante',
                ),
            ))
            ->add('tipopago', ChoiceType::class, array(
                'label' => 'Tipo de Inscripción',
                'attr' => array(
                    'col' => 'col-lg-6 col-md-6 col-sm-12',
                ),
                'choices'  => array(
                    'Inscripción + Cena' => 0,
                    'Solo Inscripción' => 1,
                    'Cena' => 2,
                ),
            ))
            ->add('area', ChoiceType::class, array(
                'label' => 'Área',
                'attr' => array(
                    'col' => 'col-lg-6 col-md-6 col-sm-12',
                ),
                'choices'  => array(
                    'Asociaciones Civiles y Fundaciones' => 'Asociaciones Civiles y Fundaciones',
                    'Cooperativas' => 'Cooperativas',
                    'Mutuales' => 'Mutuales',
                    '-' => '-',
                ),
            ))
            ->add('presentatrabajo', null, array(
                'label' => 'Presenta Trabajos? No/Sí',
                'attr' => array(
                    'col' => 'col-lg-6 col-md-6 col-sm-12',
                ),
            ))
            ->add('hotel', ChoiceType::class, array(
                'label' => 'Hoteles',
                'attr' => array(
                    'col' => 'col-lg-6 col-md-6 col-sm-12',
                ),
                'choices'  => array(
                    '-' => '-',
                    'Amerian' => 'Amerian',
                    'Niyat Urban' => 'Niyat Urban',
                    'Covadonga' => 'Covadonga',
                ),
            ))
            ->add('importe', \MWSimple\Bundle\AdminCrudBundle\Form\Type\PesoType::class, array(
                'label' => 'Importe',
                'attr' => array(
                    'col' => 'col-lg-offset-5 col-md-offset-9 col-sm-3',
                    'readonly' => true,
                ),
            ))
            ->add('menu', TextareaType::class, array(
                'label' => 'Por favor, indicar si requiere un menú especial, y en caso afirmativo, describir cuál.',
                'required'   => false,
                'attr' => array(
                    'col' => 'col-lg-12 col-md-12 col-sm-12',
                ),
            ))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Sistema\InscripcionBundle\Entity\OrgSociales'
        ]);
    }
}
