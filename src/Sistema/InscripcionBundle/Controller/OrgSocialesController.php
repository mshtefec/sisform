<?php

namespace Sistema\InscripcionBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\InscripcionBundle\Entity\OrgSociales;
use Sistema\InscripcionBundle\Form\OrgSocialesType;
use Sistema\InscripcionBundle\Form\OrgSocialesFilterType;
use Knp\Snappy\Pdf;

/**
 * OrgSociales controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/orgsociales")
 */
class OrgSocialesController extends Controller
{
    /**
     * Configuration file.
     */
    protected $config = [
        'yml' => 'Sistema/InscripcionBundle/Resources/config/OrgSociales.yml',
    ];

    /**
     * Lists all OrgSociales entities.
     *
     * @Route("/", name="orgsociales")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $this->config['filterType'] = OrgSocialesFilterType::class;
        $response = parent::indexAction($request);

        return $response;
    }

    /**
     * Creates a new OrgSociales entity.
     *
     * @Route("/", name="orgsociales_create")
     * @Method("POST")
     */
    public function createAction(Request $request)
    {
        $this->config['newType'] = OrgSocialesType::class;
        $response = parent::createAction($request);

        return $response;
    }

    /**
    * Creates a form to create a entity.
    * @return \Symfony\Component\Form\Form The form
    */
    protected function createCreateForm()
    {
        $optionsForm = [
            'action' => $this->generateUrl($this->configArray['create']),
            'method' => 'POST',
        ];
        if (is_array($this->optionsForm)) {
            foreach ($this->optionsForm as $key => $value) {
                $optionsForm[$key] = $value;
            }
        }
        $form = $this->createForm($this->configArray['newType'], $this->entity, $optionsForm);

        if ($this->configArray['saveAndAdd']) {
            $form
                ->add('save', SubmitType::class, array(
                    'translation_domain' => 'MWSimpleAdminCrudBundle',
                    'label'              => 'views.new.save',
                    'attr'               => array(
                        'class' => 'form-control btn-success',
                        'col'   => 'col-lg-2 col-md-4',
                    )
                ));
        }
        
        $form
            ->add('saveAndAdd', SubmitType::class, array(
                'translation_domain' => 'MWSimpleAdminCrudBundle',
                'label'              => 'Inscribirse',
                'attr'               => array(
                    'class' => 'form-control btn-success',
                    'col'   => 'col-lg-3 col-md-4',
                )
            ));

        return $form;
    }

    /**
     * Displays a form to create a new OrgSociales entity.
     *
     * @Route("/new", name="orgsociales_new")
     * @Method("GET")
     */
    public function newAction()
    {
        $this->config['newType'] = OrgSocialesType::class;
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a OrgSociales entity.
     *
     * @Route("/{id}", name="orgsociales_show")
     * @Method("GET")
     */
    public function showAction($id)
    {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing OrgSociales entity.
     *
     * @Route("/{id}/edit", name="orgsociales_edit")
     * @Method("GET")
     */
    public function editAction($id)
    {
        $this->config['editType'] = OrgSocialesType::class;
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing OrgSociales entity.
     *
     * @Route("/{id}", name="orgsociales_update")
     * @Method("PUT")
     */
    public function updateAction(Request $request, $id)
    {
        $this->config['editType'] = OrgSocialesType::class;
        $response = parent::updateAction($request, $id);

        return $response;
    }

    /**
     * Deletes a OrgSociales entity.
     *
     * @Route("/{id}", name="orgsociales_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $this->getConfig();
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->em = $this->getDoctrine()->getManager();
            $this->entity = $this->em->getRepository($this->configArray['repository'])->find($id);

            if (!$this->entity) {
                throw $this->createNotFoundException('Unable to find '.$this->configArray['entityName'].' entity.');
            }

            $this->preRemoveEntity();
            $this->em->remove($this->entity);
            $this->em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.delete.success');
        }

        return $this->redirectToRoute($this->configArray['new']);
    }

    /**
     * Exporter OrgSociales.
     *
     * @Route("/exporter/{format}", name="orgsociales_export")
     */
    public function getExporter($format)
    {
        $response = parent::exportCsvAction($format);

        return $response;
    }

    /**
     * Create filter form.
     * @param $filterData
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createFilterForm($filterData = null)
    {
        $form = $this->createForm($this->configArray['filterType'], $filterData, array(
            'action' => $this->generateUrl('orgsociales'),
            'method' => 'GET',
        ));

        $form
            ->add('filter', SubmitType::class, array(
                'translation_domain' => 'MWSimpleAdminCrudBundle',
                'label'              => 'views.index.filter',
                'attr'               => array(
                    'class' => 'form-control btn-success',
                    'col'   => 'col-lg-2 col-md-4',
                ),
            ));
        if ($this->configArray['sessionFilter']) {
            $form
                ->add('reset', SubmitType::class, array(
                    'translation_domain' => 'MWSimpleAdminCrudBundle',
                    'label'              => 'views.index.reset',
                    'attr'               => array(
                        'class' => 'form-control reset_submit_filters btn-danger',
                        'col'   => 'col-lg-2 col-md-4',
                    ),
                ));
        }

        return $form;
    }

    /**
     * Finds and generate PDF a Inscripcion entity.
     *
     * @Route("/get/pdf/{id}", name="inscripcion_pdf")
     * @Method("GET")
     * @Template()
     */
    public function InscripcionPdfAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaInscripcionBundle:OrgSociales')->find($id);
        
        $html = $this->renderView('SistemaInscripcionBundle:orgsociales:inscripcionPdf.html.twig', array(
            'fecha'  => $entity->getFecha(),
            'entity' => $entity,
        ));

        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml(
                $html,
                array(
                    'images'                => true,
                    'enable-external-links' => true
                )
            ),
            200,
            array(
                'images'                => true,
                'Content-Type'          => 'application/pdf',
                'Content-Disposition'   => 'attachment; filename="inscripcion_nro_' . $entity->getId() . '.pdf"'
            )
        );
    }
}
